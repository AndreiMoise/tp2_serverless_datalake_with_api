# Créer un bucket aws_s3_bucket
resource "aws_s3_bucket" "b" {
  bucket = "s3-job-offer-moise-lecoeur"
  acl    = "private"
  force_destroy = true

  tags = {
    Name        = "My bucket"
    Environment = "Dev"
  }
}


# Créer un répertoire aws_s3_bucket_object avec job_offers/raw/
resource "aws_s3_bucket_object" "object" {
  bucket = "${aws_s3_bucket.b.id}"
  key    = "job_offers/raw/"
  source = "/dev/null"
}



# Créer un event aws_s3_bucket_notification pour trigger la lambda

resource "aws_s3_bucket_notification" "bucket_notification" {
  bucket = "${aws_s3_bucket.b.id}"

  lambda_function {
    lambda_function_arn = "${aws_lambda_function.test_lambda.arn}"
    events              = ["s3:ObjectCreated:*"]
    filter_prefix       = "/job_offers/raw"
    filter_suffix       = ".csv"
  }
}